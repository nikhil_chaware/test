function function1(){
    const p1 = new Object()
    console.log(p1)
}

//function1()

function function2(){
    //creating new object
    const p1 = new Object()
    // properties
    
    p1.name = 'person1'
    p1.city = 'city1'
    p1.age = 18
    //console.log(p1)

    const obj1 = new Object()

    obj1.model = 'iPhone SE2'
    obj1.company = 'Apple'
    obj1.price = 35000

    //console.log(obj1)
}
//function2()

function printDetails(){
    console.log(`name :${this.name}`)
    console.log(`email :${this.email}`)
    
}

function function3(){
    const p1 = new Object()
    p1.name = 'person1'
    p1.email = 'person1@email.com'
    p1.age = 40
    p1.printDetails = printDetails
   // console.log(p1)
    //p1.printDetails()

    const p2 = new Object()
    p2.name = 'person2'
    p2.email = 'person2@test.com'
    p2.age = 30
    p2.printDetails = printDetails
    p2.printDetails()
    console.log(p2)
}

function3()