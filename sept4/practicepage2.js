// creating object using JSON

function function1(){
    const person = [
    { name : 'person1', email : 'person@eamil.com'},
    { name : 'person2', email : 'person2@email.com'},
    { name : 'person1', email : 'person3email.com'}
 ]
 console.log(person)
 console.log(`type of person= ${typeof(person)}`)
}

//function1()

function canvote(value){
  
     if(value['age'] < 18){
         console.log(`person ${value['name']} is not eligible`)
     }
     else{
         console.log(`person ${value['name']} is eligible`)
     }
}

function function2(){
    const p1 = { name : 'person1', email : 'person@eamil.com', age : 27}
    canvote(p1)

    const p2 = { name : 'person2', email : 'person2@email.com', age : 54}
    canvote(p2)

    const p3 = { name : 'person1', email : 'person3email.com', age : 16}
    canvote(p3)
}

 //function2()

 function function4() {
    const p1 = { name: 'person1', email: 'person1@test.com', age: 40 }
  
    console.log(`name: ${p1['name']}`)
    console.log(`email: ${p1['email']}`)
    console.log(`age: ${p1['age']}`)
  }
  
  //function4()

  function function5() {
    const p1 = { name: 'person1', email: 'person1@test.com', age: 40 }
    console.log(`name: ${p1.name}`)
    console.log(`email: ${p1.email}`)
    console.log(`age: ${p1.age}`)
  }

 // function5()

 function function6() {
    const person = {
      'first name': 'steve',
      'last name' : 'jobs'
    }
  
    console.log(`first name = ${person["first name"]}`)
    console.log(`last name = ${person['last name']}`)
  }

  //function6()


function canVotePerson() {
    console.log(`inside canVotePerson`)
    console.log(this)
    if (this['age'] >= 18) {
      console.log(`person ${this['name']} is eligible for voting`)
    } else {
      console.log(`person ${this['name']} is NOT eligible for voting`)
    }
  }

  function function7() {
    const p1 = { name: 'person1', email: 'person1@test.com', age: 40 }
  
    // PoP
    // canVotePerson(p1)
  
    // function alias
    // const myCanVote = canVotePerson
    // myCanVote(p1)
  
    // console.log(`name = ${p1.name}`)
  
    // function alias added in the person object
   p1.canVote = canVotePerson
   //console.log(p1)
  
    // OOP
    // p1.canVote(p1)
    p1.canVote()
  }
  
function7()


function function8() {
    const p1 = { name: 'person1', email: 'person1@test.com', age: 40, canVote: canVotePerson }
    const p2 = { name: 'person2', email: 'person2@test.com', age: 10, canVote: canVotePerson }
    const p3 = { name: 'person3', email: 'person3@test.com', age: 15, canVote: canVotePerson }
    
    // add the function alias into the objects
    // p1.canVote = canVotePerson
    // p2.canVote = canVotePerson
    // p3.canVote = canVotePerson
  
    p1.canVote()
    p2.canVote()
    p3.canVote()
  
  }
  
   //function8()


function function9() {
    const car1 = {
      model: 'i20',
      company: 'hyundai',
      printDetails: function() {
        console.log(`model: ${this.model}`)
        console.log(`company: ${this.company}`)
      }
    }
  
    const car2 = {
      model: 'i10',
      company: 'hyundai',
      printDetails: function() {
        console.log(`model: ${this.model}`)
        console.log(`company: ${this.company}`)
      }
    }
  
    car1.printDetails()
    car2.printDetails()
  }

 // function9()

  function function10() {
    console.log(arguments)
    console.log(this)
  }
  
 // function10()
 function7()